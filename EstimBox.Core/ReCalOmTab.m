function OmegaMat=ReCalOmTab(Omega,NumFixed,DenFixed,Plane,OPOLScalN,OPOLScalD);%% Re-eva;uation of the frequency table and the polynomials starting from Weights% Polynomials and table are SCALED FREQUENCY variables.%% INPUT PARAMETERS%%	 -	Omega					  :	SCALED pulsation vector%		- NumFixed				: Numerator list of fixed params%		- DenFixed    : Denominator list of fixed params %		- Plane       : the independant variable the estimator is started for%																	  SMode : Laplace plane estimation%																	  ZMode : Discrete plane estimation.%  - OPOLScalN : Coeffs in recurrence for the ortho pols num.%  - OPOLScalD : Coeffs in recurrence for the ortho pols den.%%% OUTPUT PARAMETERS%%		-	OmegaMat		:	Scaled Omega table used in estimation%% Modified 10/1/95 : Creation.global ZOPMode SOPModeif (Plane~=ZOPMode)&(Plane~=SOPMode)  error(' RecalOMTab : illegal plane');end;OmegaMat=zeros(length(Omega),length(NumFixed)+length(DenFixed));%% Omega matrix for the derivatives%NrNPar=sum(NumFixed);NrDPar=sum(DenFixed);LenN=length(NumFixed);LenD=length(DenFixed);% all poly's need to be calc'd anyway, reshuflle afterwards% reshuffle data if fixed assh... are present.if NrNPar+NrDPar==LenN+LenD		OmegaMat(:,1:NrNPar)=Rclc(OPOLScalN,Omega,Plane);		OmegaMat(:,NrNPar+(1:NrDPar))=Rclc(OPOLScalD,Omega,Plane);else		if NrNPar==LenN				OmegaMat(:,1:NrNPar)=Rclc(OPOLScalN,Omega,Plane);		else				OmegaMat(:,[1:NrNPar,NrNPar+NrDPar+(1:(LenN-NrNPar))])=Rclc(OPOLScalN,Omega,Plane);		end		if NrDPar==LenD		  OmegaMat(:,NrNPar+(1:NrDPar))=Rclc(OPOLScalD,Omega,Plane);  else		  OmegaMat(:,[NrNPar+(1:NrDPar),NrNPar+NrDPar+LenN-NrNPar+(1:(LenD-NrDPar))])=Rclc(OPOLScalD,Omega,Plane);		endend;