function [Omega,OmegaScale]=CalcOmega(Freq,ThePlane,FSample)
%
% Perform the scaling of the frequency vector to obtain a better
% numerical conditioning.
% 
% In the Laplace plane, the pulsation is normalized to 1 
% rather than the frequency.
%
% INPUT PARAMETERS :
% ==================
%
%  - Freq     : unscaled frequency vector. (IN HZ)
%  - ThePlane : Plane to calculatre the results in.
%  - FSample  : Sampling frequency for the Z and ZOP modes.
%
% OUTPUT PARAMETERS :
% ===================
%
%  - Omega     : unscaled pulsation vector (IN RAD/S)
%  - FreqScale : Frequency Scaling factor.
%
% Created 10/2/94 : Scaling update.

global ZOPMode SOPMode SMode ZMode

if sum(imag(Freq))>length(Freq)*eps*10 
		error('CalcOmega : complex freq encountered.');
end;

if (ThePlane==SOPMode)||(ThePlane==SMode)
  Omega     = 2*pi*Freq;
  OmegaScale = (min(Omega)+max(Omega))/2;
	Omega     = real(Omega)*sqrt(-1)/OmegaScale;

elseif (ThePlane==ZOPMode)||(ThePlane==ZMode)
  OmegaScale=FSample;
	Omega=exp(sqrt(-1)*2*pi/OmegaScale*real(Freq));

end;
fprintf(' Pulsation scaling factor = %g \n',OmegaScale);



